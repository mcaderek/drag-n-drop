/* -----------------------------------------------------------------------------
 * prototyp obiektu Item:
 */

// konstruktor - tworzy obiekt Item na podstawie przekazanej listy właściwości w formacie json
var Item = function(properties){

    this.id = parseInt(properties.id);
    this.type = properties.type;
    this.name = properties.name;
    this.path = properties.path;
    this.equiped = parseInt(properties.equiped);
    this.inventory_id = parseInt(properties.inventory_id);

    log('Przedmiot o id ' + this.id + ' (' + this.name + ') utworzony.');
};

// metoda wysyła request na serwer, jeśli dane zostaną potwierdzone
// i uaktualnione w bazie odpala odpowiednią metodę dla przedmiotu
// oba argumenty są opcjonalne, drugi argument dotyczy tylko akcji 'remove'
Item.prototype.request = function(action, newFieldId) {
    var save, inputData;

    if(action === undefined){
        if(this.type == 'potion') action = 'use';
        else if(this.equiped) action = 'remove';
        else action = 'equip';
    }

    if(action == 'remove'){
        newFieldId = newFieldId || 0;
        save = this.inventory_id;
        this.inventory_id = newFieldId;
    }
    this.task = 'update_item';
    this.action = action;
    inputData = JSON.parse(JSON.stringify(this));
    if(action == 'remove'){
        this.inventory_id = save;
    }

    var self = this;

    $.ajax({
        type: 'POST',
        url: mainPath,
        data: inputData,
        dataType: 'json',
        success: function(response) {

            if(response.status == 'ok') {
                switch(action){
                    case 'equip':
                        self.equip();
                        break;
                    case 'remove':
                        self.remove(newFieldId);
                        break;
                    case 'use':
                        self.use();
                        break;
                    case 'buy':
                        self.buy();
                        break;
                    case 'sell':
                        self.sell();
                        break;
                    // to musisz zarejestrować wszystkie później ododane metody
                    // wymagające akcji na serwerze
                    default:
                        log('Metoda nie zadeklarowana.');
                }
            } else {
                $('#itemAlerts').html(response.message);
                self.place();
            }

        },
        error: function(data, message, error) {
            // miejsce na obsługę pozostałych błedów, nie wygenerowanych ręcznie, np:
            $('#itemAlerts').html('Błąd serwera, spróbuj ponownie za chwilę...');
            self.place();
        }
    });
};

Item.prototype.equip = function(){
    var htmlItem, htmlField, previousFieldId, currentFieldId;

    htmlItem = $('#item_' + this.id);
    htmlField = htmlItem.parent();
    previousFieldId = $('#equipment_' + this.type);
    currentFieldId = htmlField.attr('id').slice(10);

    // W razie potrzeby zdejmij bieżący przedmiot i usuń z pola klasę 'occupied'
    if(previousFieldId.hasClass('occupied')){
        items[previousFieldId.children().first().attr('id').slice(5)].remove();
        previousFieldId.removeClass('occupied');
    }

    this.equiped = true;
    this.inventory_id = 0;
    this.place();


    this._clearInventoryField(currentFieldId);

    htmlItem.css({top: 0, left: 0});

    return this;
};

// metoda usuwająca przedmiot z ekwipunku / przemieszczająca przedmiot wewnątrz plecaka
Item.prototype.remove = function(newFieldId){
    var htmlItem, htmlField, currentFieldId;

    htmlItem = $('#item_' + this.id);
    htmlField = htmlItem.parent();

    if(htmlField.attr('id').slice(0,1) == 'i') {
        currentFieldId = htmlField.attr('id').slice(10);
        this._clearInventoryField(currentFieldId);
    }
    else {
        this._clearEquipmentField(this.type);
    }

    this.equiped = false;
    this.inventory_id = newFieldId || 0;
    this.place();

    this._ocupyInventoryField(newFieldId);

    htmlItem.css({top: 0, left: 0});

    return this;
};

// metoda pomocnicza (prywatna) czyści pole ekwipunku
Item.prototype._clearEquipmentField = function(id) {
    $('#equipment_' + id).removeClass('occupied');
};

// metoda pomocnicza (prywatna) czyści pole plecaka
Item.prototype._clearInventoryField = function(id) {
    $('#inventory_' + id).removeClass('occupied').droppable( "enable" );
};

// metoda pomocnicza (prywatna) zablokowywuje pole plecaka
Item.prototype._ocupyInventoryField = function(id) {
    $('#inventory_' + id).addClass('occupied').droppable( "disable" );
};

// metoda odpowiedzialna za zakup przedmiotów
Item.prototype.buy = function(){
    // tu dodaj obsługę zakupu

    log('Przedmiot o id ' + this.id + ' (' + this.name + ') kupiony.');
    return this;
};

// metoda odpowiedzialna za sprzedaż przedmiotów
Item.prototype.sell = function(){
    // tu dodaj obsługę sprzedaży

    log('Przedmiot o id ' + this.id + ' (' + this.name + ') sprzedany.');
    return this;
};

// metoda generująca kod html z obrazkami przedmiotów
Item.prototype.draw = function() {
    document.write(
        '<img id="item_' + this.id + '" class="item ' +
        this.type + '" src="' + this.path + '" alt="' + this.name + '"/>'
    );
    log('Narysowano przedmiot o id ' + this.id + ' (' + this.name + ')');
    return this;
};

// metoda umieszcza element w odpowiednim polu i zablokowywuje to pole
Item.prototype.place = function() {

    var htmlItem = $('#item_' + this.id);

    // warunkowe umieszczenie przedmiotu w ekwipunku
    if(this.equiped){
        var equipmentField = $('#equipment_' + this.type);
        if(!equipmentField.hasClass('occupied')){
            equipmentField.addClass('occupied').append(htmlItem);
            log('Przedmiot o id ' + this.id + ' (' + this.name + ') został umiejscowiony w ekwipunku.');
            return this;
        }
    }

    // ten kod wykona się tylko jeśli przedmiot nie został dodany do ekwipunku
    var htmlField = $('#inventory_' + this.inventory_id);

    if(this.inventory_id !== 0 && !htmlField.hasClass('occupied')) {
        // wyszukanie w plecaku pola o podanym id i wstawienie przedmiotu
        htmlField.addClass('occupied').append(htmlItem);
    }
    else {
        // wyszukanie w plecaku najbliższego wolnego pola i wstawienie przedmiotu
        for(var i = 1; i <= 36; i++){
            var htmlCurrentField = $('#inventory_' + i);

            if(!htmlCurrentField.hasClass('occupied')){
                htmlCurrentField.addClass('occupied').append(htmlItem);
                this.inventory_id = i;
                break;
            }
        }
    }

    htmlItem.css({top: 0, left: 0});
    log('Przedmiot o id ' + this.id + ' (' + this.name + ') został umiejscowiony w plecaku.');
    return this;
};

// metoda pomocnicza, wypisuje w konsoli listę właściwości obiektu wraz z wartościami
Item.prototype.listProperties = function() {
    var currentItemCondition = 'Bieżący stan obiektu:\n';
    for (var prop in this) {
        if( this.hasOwnProperty( prop ) ) {
            currentItemCondition += prop + " = " + this[prop] + '\n';
        }
    }
    log(currentItemCondition);
};


/* -----------------------------------------------------------------------------
 * prototyp obiektu Helmet (dziedziczy po Item)
 */

var Helmet = function(id) {
    Item.call(this, id);

    // tu możesz nadpisać oraz wstawić dodatkowe właściwości specyficzne dla Helmet
};
Helmet.prototype = Object.create(Item.prototype);
Helmet.prototype.constructor = Helmet;

// tu możesz wstawić dodatkowe metody specyficzne dla Helmet


/* -----------------------------------------------------------------------------
 * prototyp obiektu Weapon (dziedziczy po Item)
 */

var Weapon = function(id) {
    Item.call(this, id);

    // tu możesz nadpisać oraz wstawić dodatkowe właściwości specyficzne dla Weapon
};
Weapon.prototype = Object.create(Item.prototype);
Weapon.prototype.constructor = Weapon;

// tu możesz wstawić dodatkowe metody specyficzne dla Weapon


/* -----------------------------------------------------------------------------
 * prototyp obiektu Shield (dziedziczy po Item)
 */

var Shield = function(id) {
    Item.call(this, id);

    // tu możesz nadpisać oraz wstawić dodatkowe właściwości specyficzne dla Shield
};
Shield.prototype = Object.create(Item.prototype);
Shield.prototype.constructor = Shield;

// tu możesz wstawić dodatkowe metody specyficzne dla Shield


/* -----------------------------------------------------------------------------
 * prototyp obiektu Armor (dziedziczy po Item)
 */

var Armor = function(id) {
    Item.call(this, id);

    // tu możesz nadpisać oraz wstawić dodatkowe właściwości specyficzne dla Armor
};
Armor.prototype = Object.create(Item.prototype);
Armor.prototype.constructor = Armor;

// tu możesz wstawić dodatkowe metody specyficzne dla Armor


/* -----------------------------------------------------------------------------
 * prototyp obiektu Ring (dziedziczy po Item)
 */

var Ring = function(id) {
    Item.call(this, id);

    // tu możesz nadpisać oraz wstawić dodatkowe właściwości specyficzne dla Ring
};
Ring.prototype = Object.create(Item.prototype);
Ring.prototype.constructor = Ring;

// tu możesz wstawić dodatkowe metody specyficzne dla Ring


/* -----------------------------------------------------------------------------
 * prototyp obiektu Amulet (dziedziczy po Item)
 */

var Amulet = function(id) {
    Item.call(this, id);

    // tu możesz nadpisać oraz wstawić dodatkowe właściwości specyficzne dla Amulet
};
Amulet.prototype = Object.create(Item.prototype);
Amulet.prototype.constructor = Amulet;

// tu możesz wstawić dodatkowe metody specyficzne dla Amulet


/* -----------------------------------------------------------------------------
 * prototyp obiektu Boots (dziedziczy po Item)
 */

var Boots = function(id) {
    Item.call(this, id);

    // tu możesz nadpisać oraz wstawić dodatkowe właściwości specyficzne dla Boots
};
Boots.prototype = Object.create(Item.prototype);
Boots.prototype.constructor = Boots;

// tu możesz wstawić dodatkowe metody specyficzne dla Boots


/* -----------------------------------------------------------------------------
 * prototyp obiektu Potion (dziedziczy po Item)
 */

var Potion = function(id) {
    Item.call(this, id);

    // tu możesz nadpisać oraz wstawić dodatkowe właściwości specyficzne dla Boots
};
Potion.prototype = Object.create(Item.prototype);
Potion.prototype.constructor = Boots;

// metoda aktywuje przedmiot i usuwa go z plecaka
Potion.prototype.use = function() {
    var htmlItem, htmlField, currentFieldId;

    htmlItem = $('#item_' + this.id);
    htmlField = htmlItem.parent();

    currentFieldId = htmlField.attr('id').slice(10);
    this._clearInventoryField(currentFieldId);

    htmlItem.remove();

    log('Przedmiot o id ' + this.id + ' (' + this.name + ') został użyty.');
    return this;
};

// tu możesz wstawić dodatkowe metody specyficzne dla Boots

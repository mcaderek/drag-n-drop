<?php
require_once 'config.php';
require_once 'DB.php';

$db = new DB($host, $user, $pass, $database);
$rows = $db->select_multi('SELECT * FROM items');

echo json_encode($rows);

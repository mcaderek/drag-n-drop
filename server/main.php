<?php
require_once 'config.php';
require_once 'DB.php';
require_once 'Log.php';

Log::put('Main script initialized.');
Log::put('POST DATA: ' . json_encode($_POST));

$response = [
    'status' => 'error',
    'message' => 'Akcja niedozwolona. '
];

/**
 * Funkcja weryfikuje poprawność przesłanych danych i podejmuje decyzję o podjęciu akcji
 * @return bool
 */
function getPermission() {
    // tu dodaj odpowiedni kod
    return true;
}

$db = new DB($host, $user, $pass, $database);

if ($_POST['task'] == 'update_item'){

    if ($_POST['action'] == 'equip'){

        if (getPermission()) {
            // jest zezwolenie - update bazy i zmiana statusu odpowiedzi:
            $db->query('UPDATE items SET inventory_id = 0, equiped = 1 WHERE id = ' . $_POST['id']);
            $response['status'] = 'ok';
            Log::put('Przedmiot został dodany do ekwipunku');
        } else {
            // brak zezwolenia - dodanie powodu odmowy do domyślnego errora:
            $response['message'] .= 'Powód: bo tak.';
            Log::put('Przedmiot nie może zostać dodany do ekwipunku');
        }
    } else if ($_POST['action'] == 'remove') {

        if (getPermission()) {
            // jest zezwolenie - update bazy i zmiana statusu odpowiedzi:
            $db->query('UPDATE items SET inventory_id = ' . $_POST['inventory_id'] . ', equiped = 0 WHERE id = ' . $_POST['id']);
            $response['status'] = 'ok';
            Log::put('Przedmiot został dodany do ekwipunku');
        } else {
            // brak zezwolenia - dodanie powodu odmowy do domyślnego errora:
            $response['message'] .= 'Powód: bo tak.';
            Log::put('Przedmiot nie może zostać dodany do ekwipunku');
        }
    }
}

// zwrócenie odpowiedzi:
echo json_encode($response);
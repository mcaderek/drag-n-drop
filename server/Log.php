<?php

class Log {

    public static $logFile = 'log.txt';

    public static function put($message)
    {

        file_put_contents(self::$logFile, date('Y-m-d H:i:s') . ' | ' . $message . "\n", FILE_APPEND);
    }
}
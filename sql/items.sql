CREATE TABLE `items` (
  `id` int(5) NOT NULL,
  `name` varchar(30) NOT NULL,
  `type` enum('helmet','weapon','shield','armor','ring','amulet','boots','potion') NOT NULL,
  `path` varchar(255) NOT NULL,
  `equiped` tinyint(1) NOT NULL,
  `inventory_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `items`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `items`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
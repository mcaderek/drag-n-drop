INSERT INTO `items` (`type`, `name`, `path`, `equiped`, `inventory_id`) VALUES
('helmet', 'zwykły hełm', 'graphics/items/helmet.png', 0, 1),
('weapon', 'zwykła broń', 'graphics/items/weapon.png', 0, 2),
('shield', 'zwykła tarcza', 'graphics/items/shield.png', 0, 3),
('armor', 'zwykła zbroja', 'graphics/items/armor.png', 0, 4),
('ring', 'zwykły pierścień', 'graphics/items/ring.png', 0, 5),
('amulet', 'zwykły amulet', 'graphics/items/amulet.png', 0, 6),
('boots', 'zwykłe buty', 'graphics/items/boots.png', 0, 7),
('potion', 'zwykła mikstura', 'graphics/items/potion.png', 0, 8);